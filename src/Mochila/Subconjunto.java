package Mochila;

import java.util.HashSet;
import java.util.Set;

public class Subconjunto 
{
	private Set<Objeto> _objetos;
	private Instancia _instancia;
	
	public Subconjunto(Instancia inst)
	{
		_instancia = inst;
		_objetos = new HashSet<>();	
	}
	
	public void agregar(Objeto obj)
	{
		_objetos.add(obj);
	}
	
	public void sacar(Objeto obj) 
	{
		_objetos.remove(obj);	
	}
	
	public Subconjunto clonar()
	{
		Subconjunto s = new Subconjunto(_instancia);
		for (Objeto o: _objetos)
			s.agregar(o);
		return s;
	}
	
	@Override
	public String toString()
	{
		String s = "{  ";
		for (Objeto o : _objetos)
		{
			s += o.nombre() + " ";
		}
		return s + "}";
	}
	
	public boolean tieneMayorBeneficioQue (Subconjunto s)
	{
		return s == null || this.beneficio() >= s.beneficio();
	}
	
	public boolean entra(Objeto obj)
	{
		return this.peso() + obj.peso()
		   <= _instancia.capacidad();
	}

	public double beneficio() 
	{
		double ret = 0;
		for (Objeto o: _objetos)
			ret += o.beneficio();
		return ret;
	}
	
	public double peso()
	{
		double ret = 0;
		for (Objeto o: _objetos)
			ret += o.peso();
		return ret;
	}
}
