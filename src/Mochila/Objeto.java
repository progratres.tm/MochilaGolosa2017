package Mochila;

public class Objeto 
{
	private String nombre;
	private double peso;
	private double beneficio;
	
	public Objeto(String n, double p, double b)
	{
		nombre = n;
		peso = p;
		beneficio = b;
	}
	
	public String nombre()
	{
		return nombre;
	}
	
	public double peso()
	{
		return peso;
	}

	public double beneficio()
	{
		return beneficio;
	}
}
