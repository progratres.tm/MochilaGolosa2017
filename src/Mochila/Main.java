package Mochila;

public class Main 
{
	public static void main(String[] args) 
	{	
		Instancia instancia = InstanciaCampamento();
		
		SolverGoloso goloso = new SolverGoloso(instancia);
		
		mostrar(goloso.resolverPorPeso(), "por Peso");
		mostrar(goloso.resolverPorBeneficio(), "por Beneficio");
		mostrar(goloso.resolverPorCociente(), "por Cociente");
	}

	private static Instancia InstanciaCampamento() 
	{
		double peso = 15;
		Instancia instancia = new Instancia(peso);
		
		instancia.agregarObjeto(new Objeto("Linterna",2,6));
		instancia.agregarObjeto(new Objeto("Encendedor",1,10));
		instancia.agregarObjeto(new Objeto("Comida",5,10));
		instancia.agregarObjeto(new Objeto("Bebida",3,8));
		instancia.agregarObjeto(new Objeto("Ropa",3,4));
		instancia.agregarObjeto(new Objeto("Repelente",1,2));
		instancia.agregarObjeto(new Objeto("Bolsa de Dormir",5,5));
		instancia.agregarObjeto(new Objeto("Cubiertos",1,2));
		instancia.agregarObjeto(new Objeto("Plato",2,2));
		instancia.agregarObjeto(new Objeto("Taza",1,1));
		instancia.agregarObjeto(new Objeto("Cantimplora",1,7));
		instancia.agregarObjeto(new Objeto("Equipo de Mate",4,6));
		return instancia;
	}
	
	private static void mostrar(Subconjunto solucion, String metodo) 
	{
		System.out.println("Solucion Goloso " + metodo);
		System.out.println("Beneficio = " + solucion.beneficio() );
		System.out.println( " Subconjunto = " + solucion);		
	}
}
