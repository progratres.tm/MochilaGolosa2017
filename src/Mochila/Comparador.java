package Mochila;

import java.util.Comparator;

public interface Comparador extends Comparator<Objeto>
{
	public int compare(Objeto uno, Objeto otro);
}
