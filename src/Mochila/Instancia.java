package Mochila;

import java.util.ArrayList;

public class Instancia 
{
	private double capacidad;
	private ArrayList<Objeto> objetos;
	
	public Instancia(double capacidadMax)
	{
		capacidad = capacidadMax;
		objetos = new ArrayList<>();
	}
	
	public double capacidad()
	{
		return capacidad;
	}
	
	public void agregarObjeto (Objeto obj)
	{
		objetos.add(obj);
	}
	
	public Objeto objeto(int i)
	{
		return objetos.get(i);
	}
	
	public int cantidadDeObjetos()
	{
		return objetos.size();
	}
}
