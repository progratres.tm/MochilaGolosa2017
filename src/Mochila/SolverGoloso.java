package Mochila;

import java.util.ArrayList;
import java.util.Collections;

public class SolverGoloso 
{
	// Un solver est� siempre asociado con una instancia
	private Instancia _instancia;

	public SolverGoloso(Instancia instancia)
	{
		_instancia = instancia;
	}

	public Subconjunto resolverPorBeneficio()
	{
		ArrayList<Objeto> ordenados = ordenar(new Comparador()
		{
			@Override
			public int compare(Objeto uno, Objeto otro) 
			{
				if( uno.beneficio() > otro.beneficio() )
					return -1;
				else if( uno.beneficio() == otro.beneficio() )
					return 0;
				else
					return 1;
			}
		});
		return generarSolucion(ordenados);
	}

	public Subconjunto resolverPorPeso()
	{
		ArrayList<Objeto> ordenados = ordenar(new Comparador()
		{
			@Override
			public int compare(Objeto uno, Objeto otro) 
			{
				if( uno.peso() < otro.peso() )
					return -1;
				else if( uno.peso() == otro.peso() )
					return 0;
				else
					return 1;
			}
		});
		return generarSolucion(ordenados);
	}

	public Subconjunto resolverPorCociente()
	{
		ArrayList<Objeto> ordenados = ordenar(new Comparador()
		{
			@Override
			public int compare(Objeto uno, Objeto otro) 
			{
				double cocienteUno = uno.beneficio() / uno.peso();
				double cocienteOtro = otro.beneficio() / otro.peso();

				if( cocienteUno > cocienteOtro )
					return -1;
				else if( cocienteUno == cocienteOtro )
					return 0;
				else
					return 1;
			}			
		});
		return generarSolucion(ordenados);
	}

	private ArrayList<Objeto> ordenar(Comparador comparador)
	{
		// Ponemos los objetos en un ArrayList
		ArrayList<Objeto> ret = new ArrayList<Objeto>();
		for(int i=0; i<_instancia.cantidadDeObjetos(); ++i)
			ret.add( _instancia.objeto(i) );

		// Los ordenamos y los retornamos ordenados
		Collections.sort(ret, comparador);
		return ret;
	}

	private Subconjunto generarSolucion(ArrayList<Objeto> ordenados)
	{
		Subconjunto subconjunto = new Subconjunto(_instancia);
		for(Objeto obj: ordenados)
			if( subconjunto.entra(obj) )
				subconjunto.agregar(obj);

		return subconjunto;
	}
}
